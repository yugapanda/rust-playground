use std::io::Read;

fn main() {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let mut iter = buf.split_whitespace();
    let cards = iter.next().unwrap().chars().collect::<Vec<char>>();
    let counts = cards.iter().map(|x| count(&cards, x)).collect::<Vec<usize>>();
    let hand_point = hand(&counts);
    let finally = if cards.contains(&'*') {
        match hand_point {
            0 => 1,
            1 => 3,
            2 => 3,
            3 => 4,
            _ => 0
        }
    } else {
        hand_point
    };

    let ans = match finally {
        0 => "NoPair",
        1 => "OnePair",
        2 => "TwoPair",
        3 => "ThreeCard",
        4 => "FourCard",
        _ => ""
    };

    println!("{}", ans)
}


fn count(cards: &Vec<char>, target: &char) -> usize {
    cards.iter().fold(0, |acc, x| {
        if x.eq(target) {
            acc + 1
        } else {
            acc
        }
    })
}

fn hand(counts: &Vec<usize>) -> usize {
    let mut c = counts.clone();
    c.sort();
    match c {
        ref x if x.eq(&vec!(1, 1, 1, 1)) => 0,
        ref x if x.eq(&vec!(1, 1, 2, 2)) => 1,
        ref x if x.eq(&vec!(2, 2, 2, 2)) => 2,
        ref x if x.eq(&vec!(1, 3, 3, 3)) => 3,
        ref x if x.eq(&vec!(4, 4, 4, 4)) => 4,
        _ => 0
    }
}
