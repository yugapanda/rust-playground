use std::io::Read;

fn main() {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let mut iter = buf.split_whitespace();
    let n: usize = iter.next().unwrap().parse().unwrap();

    let all = (0..n).map(|_| {
        (iter.next().unwrap(),
        iter.next().unwrap().parse().unwrap())
    }).collect::<Vec<(&str, usize)>>();

    let ans: usize = all.iter().map(|x| {
        calc_point(&x.0.to_string(), x.1)
    }).sum();

    println!("{}", ans);


}


fn calc_point(date: &String, amount: usize) -> usize {
    match date {
        x if x.contains("5") => ((amount as f32) * 0.05) as usize,
        x if x.contains("3") => ((amount as f32) * 0.03) as usize,
        x => ((amount as f32) * 0.01) as usize,
    }
}