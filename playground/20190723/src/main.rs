use std::io::Read;

fn main() {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let mut iter = buf.split_whitespace();
    let n: usize = iter.next().unwrap().parse().unwrap();
    
    let words: Vec<(&str, &str)> = (0..n).map(|_| {
        (
            iter.next().unwrap(), 
            iter.next().unwrap()
        )
    }
    ).collect();


    let maped = words.iter().map(|x| {
        match x {
            y if y.0.len() != y.1.len() => 0,
            y => diff(*y)
        }
    });

    let sum: usize = maped.sum();
    println!("{}", sum)

}

fn diff(words: (&str, &str)) -> usize {
    let w1: Vec<char> = words.0.to_string().chars().collect::<Vec<char>>();
    let w2: Vec<char> = words.1.to_string().chars().collect::<Vec<char>>();
    let wt: Vec<(char, char)> = w1.into_iter().zip(w2.into_iter()).collect::<Vec<(char, char)>>();
    diffDo(wt, 2)
}

fn diffDo(words1: Vec<(char, char)>,  acc: usize) -> usize {
    if acc == 0 {
        return 0;
    }
    let cons = words1.split_first();

    match cons {
        None => acc,
        Some(x) if (x.0).0 == (x.0).1 => diffDo(x.1.to_vec(), acc),
        Some(x) if (x.0).0 != (x.0).1 => diffDo(x.1.to_vec(), acc -1),
        x => acc
    }
}