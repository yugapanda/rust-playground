use std::io::Read;

fn main() {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let mut iter = buf.split_whitespace();
    let hate_num_str  = iter.next().unwrap().chars().collect::<Vec<char>>();
    let hate_num = hate_num_str.first().unwrap();
    let room_total = iter.next().unwrap().parse().unwrap();

    let rooms = (0..room_total).map(|_| {
        iter.next().unwrap().to_string().chars().collect()
    }).collect::<Vec<Vec<char>>>();

    let ok: Vec<String> = rooms.iter()
        .filter(|x| !x.contains(hate_num))
        .map(|x| x.iter().collect())
        .collect();

    if ok.is_empty() {
        println!("{}", "none")
    } else {
        ok.iter().for_each(|x| {
            println!("{}", x);
        })
    }


}
