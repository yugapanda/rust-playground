use std::io::Read;


fn sub() {
    let mut buf = String::new();
    // let buf = "3 + 3 = x";
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let mut iter = buf.split_whitespace();
    let aexp = AExp {
        a1: iter.next().unwrap(),
        op: iter.next().unwrap(),
        a2: iter.next().unwrap(),
        eq: iter.next().unwrap(),
        ans: iter.next().unwrap(),
    };

    let ans = match aexp {
        AExp { a1, op: "+", a2, eq: "=", ans: "x" } => plus(a1, a2),
        AExp { a1, op: "-", a2, eq: "=", ans: "x" } => minus(a1, a2),
        AExp { a1, op: "+", a2: "x", eq: "=", ans } => minus(ans, a1),
        AExp { a1, op: "-", a2: "x", eq: "=", ans } => minus(a1, ans),
        AExp { a1: "x", op: "+", a2, eq: "=", ans } => minus(ans, a2),
        AExp { a1: "x", op: "-", a2, eq: "=", ans } => plus(ans, a2),
        _ => 0
    };
    println!("{}", ans)
}

fn plus(a1: &str, a2: &str) -> usize {
    a1.parse::<usize>().unwrap() + a2.parse::<usize>().unwrap()
}

fn minus(a1: &str, a2: &str) -> usize {
    a1.parse::<usize>().unwrap() - a2.parse::<usize>().unwrap()
}

struct AExp<'a> {
    a1: &'a str,
    op: &'a str,
    a2: &'a str,
    eq: &'a str,
    ans: &'a str,

}