use std::io::Read;


fn main() {
    let mut buf = String::new();
    // let buf = "3 + 3 = x";
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let iter = buf.split_whitespace().collect::<Vec<&str>>();

    let ans = match iter.as_slice() {
        [a1, "+", a2, "=", "x" ] => plus(a1, a2),
        [a1, "-", a2, "=", "x" ] => minus(a1, a2),
        [a1, "+", "x", "=", ans ] => minus(ans, a1),
        [a1, "-", "x",  "=", ans ] => minus(a1, ans),
        ["x", "+", a2, "=", ans ] => minus(ans, a2),
        ["x", "-", a2, "=", ans ] => plus(ans, a2),
        _ => 0
    };
    println!("{}", ans)
}

fn plus(a1: &str, a2: &str) -> usize {
    a1.parse::<usize>().unwrap() + a2.parse::<usize>().unwrap()
}

fn minus(a1: &str, a2: &str) -> usize {
    a1.parse::<usize>().unwrap() - a2.parse::<usize>().unwrap()
}
